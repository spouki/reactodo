import React from 'react';
import ReactDOM from 'react-dom';

// Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";
// Bootstrap Bundle JS
import "bootstrap/dist/js/bootstrap.bundle.min";

class SelfEditingInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editing: false,
            onChangeValue: props.onChangeTask,
            onUpdate: props.onBlur,
        };
        this.toggleEditing = this.toggleEditing.bind(this);
    }

    componentDidMount() {
        // Voir pour mettre le fade ici
    }

    componentWillUnmount() {
        // Voir pour mettre un fade ici aussi
    }

    toggleEditing() {
        console.log(`Toggling field ${this.state.editing}`);
        if (this.state.editing) {
            this.state.onUpdate();
        } else {
            if (this.state.crossed) {
                return ;
            }
        }
        this.setState(
            {editing: !this.state.editing},
        )
    }

    render() {
        if (this.props.crossed) {
            return(
                <span
                    className={"input-group-text form-control"}
                >
                    <strike>
                        {this.props.value}
                    </strike>
            </span>
            );
        }
        if (this.state.editing) {
            return(
                <input
                    className={"form-control"}
                    value={this.props.value}
                    onBlur={this.toggleEditing}
                    onChange={this.state.onChangeValue}
                />
            );
        }
        return(
            <span
                className={"input-group-text form-control"}
                onClick={this.toggleEditing}
            >
                {this.props.value}
            </span>
        );
    }
}

class SelfEditingNavTabButton extends React.Component {
    constructor(props) {
        super(props);
        //
        this.state = {
            name: props.name,
            id: props.id,
            className: props.className,
            tabIndex: props.tabIndex,
            shallowValue: props.name,
            changeSelectedTab: props.onClickHandler,
            editing: false,
            onUpdate: props.onBlur,
            onChangeValue: props.onChangeTabName,
        };
        this.toggleEditing = this.toggleEditing.bind(this);
    }

    toggleEditing() {
        if (this.state.editing) {
            this.state.onUpdate();
        }
        this.setState(
            {editing: !this.state.editing},
        )
    }

    render() {
        if (this.props.editing) {
            return (
                <li className={"nav-item"} role={"presentation"} key={this.state.tabIndex}>
                    <input
                        type="text"
                        value={this.props.name}
                        onBlur={this.toggleEditing}
                        onChange={this.state.onChangeValue}
                    />
                </li>
            );
        }
        return (
            <li className={"nav-item"} role={"presentation"} key={this.state.tabIndex}>
                <button
                    className={this.state.className}
                    id={this.state.id}
                    data-bs-toggle={"tab"}
                    data-bs-target={"#"+this.state.id+"-pane"}
                    type={"button"} role={"tab"}
                    aria-controls={this.state.id+"-pane"}
                    aria-selected={"true"}
                    onClick={this.state.changeSelectedTab}
                    // onChange={this.state.onChangeValue}
                >
                    {this.props.name}
                </button>
            </li>
        );
    }
}

class ToDo extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            collections: [
                {
                    name: "Example 1",
                    shallowName: "Example 1",
                    tabMenuDisplay: false,
                    id: 0,
                    tasks: [{
                        taskText: "Coucou, suis-je une task ?",
                        shallowValue: "Coucou, suis-je une task ?",
                        crossed: false,
                    }]
                },
                {
                    name: "Example 2",
                    shallowName: "Example 2",
                    tabMenuDisplay: false,
                    id: 1,
                    tasks: [{
                        taskText: "Coucou, je suis une task",
                        shallowValue: "Coucou, je suis une task",
                        crossed: false,
                    }]
                },
            ],
            selectedCollectionIndex:0,
            addTaskField: "",
        };

        this.addNewTask = this.addNewTask.bind(this);
        this.handleChangeNewTask = this.handleChangeNewTask.bind(this);
        this.changeSelectedTab = this.changeSelectedTab.bind(this);
        this.addTabCollection = this.addTabCollection.bind(this);
    }

    toggleTaskCheck(taskIndex) {
        const collections = this.state.collections.slice();
        const tasks = collections[this.state.selectedCollectionIndex].tasks.slice();
        tasks[taskIndex].crossed = !tasks[taskIndex].crossed;
        collections[this.state.selectedCollectionIndex].tasks = tasks;
        this.setState({collections: collections});
    }

    addNewTask() {
        if (this.state.addTaskField.length < 1) return ;
        const collections = this.state.collections.slice();
        const tasks = collections[this.state.selectedCollectionIndex].tasks.slice();
        tasks.push({taskText: this.state.addTaskField, shallowValue: this.state.addTaskField, crossed: false});
        collections[this.state.selectedCollectionIndex].tasks = tasks;
        this.setState(
            {collections:collections}
        );
        this.setState(
            {addTaskField: ""}
        );
    }

    handleChangeNewTask(evt) {
        console.log(`Trying to add new task with value : ${evt.target.value}`);
        this.setState(
            {addTaskField: evt.target.value}
        );
    }

    removeTask(taskIndex) {
        console.log(`Removing task number ${taskIndex}`);
        const collections = this.state.collections.slice();
        const tasks = collections[this.state.selectedCollectionIndex].tasks.slice();
        tasks.splice(taskIndex, 1);
        collections[this.state.selectedCollectionIndex].tasks = tasks;
        this.setState(
            {collections: collections}
        );
    }

    handleChangeTaskValue(evt, taskIndex) {
        const collections = this.state.collections.slice();
        const tasks = collections[this.state.selectedCollectionIndex].tasks.slice();
        tasks[taskIndex].shallowValue = evt.target.value;
        collections[this.state.selectedCollectionIndex].tasks = tasks;
        this.setState(
            {collections:collections}
        );
    }

    handleChangeTabValue(evt, tabIndex) {
        console.log(`Change tab value : ${evt.target.value}`);
        const collections = this.state.collections.slice();
        const tab = collections[tabIndex];
        tab.shallowName = evt.target.value;
        collections[tabIndex] = tab;
        this.setState(
            {collections:collections}
        );
    }

    updateTaskValue(taskIndex) {
        const collections = this.state.collections.slice();
        const tasks = collections[this.state.selectedCollectionIndex].tasks.slice();
        console.log(`Updating real value "${tasks[taskIndex].taskText}" with shallow value "${tasks[taskIndex].shallowValue}"`);
        tasks[taskIndex].taskText = tasks[taskIndex].shallowValue;
        collections[this.state.selectedCollectionIndex].tasks = tasks;
        this.setState(
            {collections:collections}
        );
    }

    updateTabValue(tabIndex) {
        const collections = this.state.collections.slice();
        const tab = collections[tabIndex];
        console.log(`Update tab Value for ${tab.shallowName}`);
        tab.name = tab.shallowName;
        collections[tabIndex] = tab;
        this.setState(
            {collections:collections}
        );
        this.toggleTabValueEditing(tabIndex);
    }

    changeSelectedTab(tabIndex) {
        console.log(`ChangeSelectedTab called with value ${tabIndex}`);
        if (tabIndex === this.state.selectedCollectionIndex) {
            this.toggleTabValueEditing(tabIndex);
        } else {
            // This should be made another way, but I don't know how for the moment
            const collection = this.state.collections.slice();
            const cleaned_collection = collection.map(
                (tab) => {
                    if (tab.id !== tabIndex) {
                        tab.tabMenuDisplay = false;
                    }
                }
            );
            this.setState(
                {collection: cleaned_collection}
            );
            this.setState(
                {selectedCollectionIndex: tabIndex}
            );
        }
    }

    toggleTabValueEditing(tabIndex) {
        const collection = this.state.collections.slice();
        collection[tabIndex].tabMenuDisplay = !collection[tabIndex].tabMenuDisplay;
        this.setState(
            {collections: collection}
        );
    }

    addTabCollection(evt) {
        console.log("Ping pong !");
        const collections = this.state.collections;
        collections.push(
            {
                name: "Click here to rename",
                shallowName: "Click here to rename",
                tabMenuDisplay: false,
                id: this.state.collections.length,
                tasks: []
            }
        );
        this.setState(
            {collections: collections}
        );
    }

    render() {
        const tab_collections = this.state.collections.slice();
        const tabs = tab_collections.map(
            (tabInfo, tabIndex) => {
                let className = "nav-link"
                if (tabIndex === this.state.selectedCollectionIndex) {
                    className += " active";
                }
                return (
                    <SelfEditingNavTabButton
                        name={tabInfo.shallowName}
                        id={tabInfo.index+"-tab"}
                        className={className}

                        tabIndex={tabIndex}

                        onChangeTabName = {
                            (evt) => {
                                console.log(`DO YOU DO ?`);
                                this.handleChangeTabValue(evt, tabIndex);
                            }
                        }

                        onBlur={ () => { this.updateTabValue(tabIndex) } }
                        onClickHandler={ () => { this.changeSelectedTab(tabIndex) } }
                        editing={tabInfo.tabMenuDisplay}
                    >
                    </SelfEditingNavTabButton>
                );
            }
        );

        const tasks_collection = tab_collections[this.state.selectedCollectionIndex].tasks.slice();
        const tasks = tasks_collection.map(
            (taskInfo, taskIndex) => {
                return(
                    <li key={taskIndex} className={"list-group-item"}>
                        <div className={"input-group"}>
                            <div className={"input-group-text"}>
                                <input
                                    className={"form-check-input"}
                                    type={"checkbox"}
                                    checked={taskInfo.crossed}
                                    onClick={
                                        (evt) => {
                                            this.toggleTaskCheck(taskIndex)
                                        }
                                    }
                                />
                            </div>
                            <SelfEditingInput
                                value={taskInfo.shallowValue}
                                onChangeTask={
                                    (evt) => {
                                        this.handleChangeTaskValue(evt, taskIndex)
                                    }
                                }
                                onBlur={
                                    () => {
                                        this.updateTaskValue(taskIndex);
                                    }
                                }
                                crossed={taskInfo.crossed}
                            >
                            </SelfEditingInput>
                            <button
                                type={"button"}
                                onClick={() => this.removeTask(taskIndex)}
                                className={"btn btn-danger input-group-text"}
                            >
                                X
                            </button>
                        </div>
                    </li>
                );
            }
        );

        return (
            <div className={"container-sm"}>
                <div>
                    <h1>ToDo</h1>
                </div>

                <ul className={"nav nav-tabs"} id={"myTab"} role={"tablist"}>
                    {tabs}
                    <li className={"nav-item"} role={"presentation"} key={"why_not"}><button className={"nav-link"} onClick={this.addTabCollection}>+</button></li>
                </ul>

                <div id={"task_add"} className={"input-group"}>
                    <span className={"input-group-text"}>Add a new task :</span>
                    <input type="text" placeholder={"New task..."} value={this.state.addTaskField} onChange={this.handleChangeNewTask} className={"form-control"} />
                    <button
                        onClick={this.addNewTask}
                        className={"btn btn-success"}
                    >
                        Add task
                    </button>
                </div>
                <hr/>
                <div>
                    <ul className={"list-group"}>
                        {tasks}
                    </ul>
                </div>
                <hr/>
            </div>
        );
    }
}



const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<ToDo />);
